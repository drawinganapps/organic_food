import 'package:flutter/material.dart';

class ColorHelper {
  static Color white = const Color.fromRGBO(255,255,255, 1);
  static Color grey = const Color.fromRGBO(237, 227, 219, 1);
  static Color primary = const Color.fromRGBO(239, 127, 46, 1);
  static Color secondary = const Color.fromRGBO(234, 153, 48, 1.0);
  static Color tertiary = const Color.fromRGBO(252, 232, 216, 1);
  static Color quaternary = const Color.fromRGBO(254, 238, 225, 1);
  static Color yellow = const Color.fromRGBO(255, 144, 18, 1);
  static Color lightGreen = const Color.fromRGBO(221, 227, 202, 1);
  static Color darkGreen = const Color.fromRGBO(61, 107, 49, 1);
  static Color dark = const Color.fromRGBO(69, 68, 67, 1);
}
