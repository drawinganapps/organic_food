import 'package:flutter/material.dart';
import 'package:organic_food_app/helper/color_helper.dart';
import 'package:organic_food_app/models/item_model.dart';
import 'package:organic_food_app/sources/dummy_data.dart';
import 'package:organic_food_app/widgets/item_quantity_widget.dart';

class ItemPage extends StatelessWidget {
  const ItemPage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    ItemModel item = DummyData.items[1];
    return ListView(
      children: [
        Container(
          padding: EdgeInsets.only(
              left: widthSize * 0.05,
              right: widthSize * 0.05,
              bottom: heightSize * 0.02),
          decoration: BoxDecoration(
              color: ColorHelper.lightGreen,
              borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(35),
                bottomRight: Radius.circular(35),
              )),
          height: heightSize * 0.5,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset('assets/images/${item.icon}',
                  height: heightSize * 0.45),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: widthSize * 0.02),
                        child: Icon(Icons.local_shipping_rounded,
                            color: ColorHelper.darkGreen),
                      ),
                      Text('Free shipping',
                          style: TextStyle(
                            color: ColorHelper.darkGreen,
                          ))
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: widthSize * 0.01),
                        child: Icon(
                          Icons.star_rounded,
                          color: ColorHelper.yellow,
                        ),
                      ),
                      Text(item.rate.toString(),
                          style: TextStyle(
                            color: ColorHelper.dark.withOpacity(0.8),
                          ))
                    ],
                  )
                ],
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(
              top: heightSize * 0.03, bottom: heightSize * 0.02),
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Text(item.name,
              style: TextStyle(
                  color: ColorHelper.dark,
                  fontSize: 28,
                  fontWeight: FontWeight.w700)),
        ),
        Container(
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Text(DummyData.description,
              maxLines: 7,
              textAlign: TextAlign.justify,
              style: TextStyle(
                  color: ColorHelper.dark,
                  fontSize: 18,
                  height: 1.5,
                  wordSpacing: 1.2,
                  fontWeight: FontWeight.w500)),
        ),
        Container(
            margin: EdgeInsets.only(
                top: heightSize * 0.03, bottom: heightSize * 0.03),
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text('\$',
                        style: TextStyle(
                            fontSize: widthSize * 0.04,
                            color: ColorHelper.yellow)),
                    Container(
                      margin: EdgeInsets.only(
                          left: widthSize * 0.005, right: widthSize * 0.01),
                      child: Text(item.price.toString(),
                          style: TextStyle(
                              fontSize: widthSize * 0.08,
                              fontWeight: FontWeight.w800,
                              color: ColorHelper.yellow)),
                    ),
                    Text('Kg',
                        style: TextStyle(
                            fontSize: widthSize * 0.04,
                            color: ColorHelper.dark.withOpacity(0.6))),
                  ],
                ),
                const ItemQuantityWidget(),
              ],
            )),
        Container(
          padding:
              EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Container(
            width: widthSize,
            height: heightSize * 0.06,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    ColorHelper.primary,
                    ColorHelper.secondary,
                  ],
                ),
                borderRadius: BorderRadius.circular(15)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(right: widthSize * 0.015),
                  child: const Icon(Icons.shopping_bag_rounded),
                ),
                Text('Add To Cart',
                    style: TextStyle(
                        color: ColorHelper.white,
                        fontSize: 14,
                        fontWeight: FontWeight.w600))
              ],
            ),
          ),
        )
      ],
    );
  }
}
