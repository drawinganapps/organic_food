import 'package:flutter/cupertino.dart';
import 'package:go_router/go_router.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organic_food_app/helper/color_helper.dart';
import 'package:organic_food_app/routes/AppRoutes.dart';
import 'package:organic_food_app/sources/dummy_data.dart';
import 'package:organic_food_app/widgets/banner_widget.dart';
import 'package:organic_food_app/widgets/filter_widget.dart';
import 'package:organic_food_app/widgets/item_card_widget.dart';
import 'package:organic_food_app/widgets/search_widget.dart';

class MainPage extends StatelessWidget {
  const MainPage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return ListView(
      physics: const BouncingScrollPhysics(),
      children: [
        Container(
          margin: EdgeInsets.only(top: heightSize * 0.02),
          padding: EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Discover', style: GoogleFonts.abrilFatface(
                color: ColorHelper.dark,
                fontWeight: FontWeight.w600,
                fontSize: widthSize * 0.1
              )),
              Container(
                margin: EdgeInsets.only(top: heightSize * 0.01),
                child: Text('Explore the best fresh food', style: TextStyle(
                  fontSize: widthSize * 0.06,
                  fontWeight: FontWeight.w200,
                  color: ColorHelper.dark.withOpacity(0.8)
                )),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: heightSize * 0.02, bottom: heightSize * 0.02),
          padding: EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: const SearchWidget(),
        ),
        Container(
          padding: EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: const BannerWidget(),
        ),
        Container(
          height: 30,
          margin: EdgeInsets.only(top: heightSize * 0.02, bottom: heightSize * 0.02),
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: List.generate(DummyData.filter.length, (index){
              return Container(
                margin: EdgeInsets.only(left: widthSize * 0.05),
                child: FilterWidget(isSelected: index == 0, filter: DummyData.filter[index]),
              );
            }),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: heightSize * 0.02, bottom: heightSize * 0.02),
          padding: EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
          child: GridView.count(
            shrinkWrap: true,
            crossAxisCount: 2,
            crossAxisSpacing: 15,
            mainAxisSpacing: 15,
            childAspectRatio: 0.75,
            physics: const BouncingScrollPhysics(),
            children: List.generate(DummyData.items.length, (index) {
              return GestureDetector(
                child: ItemCardWidget(
                  item: DummyData.items[index],
                ),
                onTap: () => context.go(AppRoutes.DETAILS),
              );
            }),
          ),
        ),
      ],
    );
  }

}