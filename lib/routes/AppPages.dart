import 'package:go_router/go_router.dart';
import 'package:organic_food_app/routes/AppRoutes.dart';
import 'package:organic_food_app/screens/item_screen.dart';
import 'package:organic_food_app/screens/main_screen.dart';

class AppPage {
// GoRouter configuration
  static var routes = GoRouter(
    initialLocation: AppRoutes.HOME,
    routes: [
      GoRoute(
        path: AppRoutes.HOME,
        builder: (context, state) => const MainScreen(),
      ),
      GoRoute(
        path: AppRoutes.DETAILS,
        builder: (context, state) => const ItemScreen(),
      )
    ],
  );
}
