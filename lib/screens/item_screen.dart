import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:organic_food_app/helper/color_helper.dart';
import 'package:organic_food_app/pages/item_page.dart';
import 'package:organic_food_app/routes/AppRoutes.dart';

class ItemScreen extends StatelessWidget {
  const ItemScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.lightGreen,
        elevation: 0,
        leading: Container(
          padding: EdgeInsets.only(left: widthSize * 0.03),
          child: GestureDetector(
            onTap: () => context.go(AppRoutes.HOME),
            child: Icon(Icons.arrow_back_ios_new_rounded, color: ColorHelper.dark, size: 35),
          ),
        ),
        actions: [
          Container(
            decoration: const BoxDecoration(
                shape: BoxShape.circle
            ),
            padding: EdgeInsets.only(right: widthSize * 0.05),
            child: Image.asset('assets/icons/woman.png', width: 45, fit: BoxFit.fitWidth),
          )
        ],
      ),
      body: const ItemPage(),
    );
  }

}