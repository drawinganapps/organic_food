import 'package:flutter/material.dart';
import 'package:organic_food_app/helper/color_helper.dart';
import 'package:organic_food_app/pages/main_page.dart';
import 'package:organic_food_app/widgets/bottom_navigation_widget.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorHelper.quaternary,
        elevation: 0,
        leading: Container(
          padding: EdgeInsets.only(left: widthSize * 0.03),
          child: Icon(Icons.menu, color: ColorHelper.dark, size: 35),
        ),
        actions: [
          Container(
            decoration: const BoxDecoration(
              shape: BoxShape.circle
            ),
            padding: EdgeInsets.only(right: widthSize * 0.05),
            child: Image.asset('assets/icons/woman.png', width: 45, fit: BoxFit.fitWidth),
          )
        ],
      ),
      body: const MainPage(),
      bottomNavigationBar: const BottomNavigationWidget(selectedMenu: 0),
    );
  }

}