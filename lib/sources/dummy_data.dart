import 'package:organic_food_app/models/filter_model.dart';
import 'package:organic_food_app/models/item_model.dart';

class DummyData {
  static List<FilterModel> filter = [
    FilterModel(1, 'All'),
    FilterModel(2, 'Cucumbers'),
    FilterModel(3, 'Mango'),
    FilterModel(4, 'Avocado'),
    FilterModel(5, 'Limes'),
    FilterModel(6, 'Berry'),
  ];

  static List<ItemModel> items = [
    ItemModel(1, 'Watermelon', 'watermelon.png', 3.6, 4.5),
    ItemModel(1, 'Avocado', 'avocado.png', 5.6, 4.8),
  ];

  static String description = 'You boil some water while you desperately tear open the packet. The water bubbles away madly as you reach to cut the taste maker within the two-minute noodle packet. Two minutes, maybe three minutes later, you’re burning your tongue as you wallop down the noodles. What you’ve experienced is an instant result. And readers too want to experience that very same instant result with your content. They don’t want to waffle through mountains of information. They want a nice, quick bite to begin with. Of course, it helps if you have the right parameters in place.';
}