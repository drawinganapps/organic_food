import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:organic_food_app/helper/color_helper.dart';

ThemeData defaultTheme = ThemeData(
    brightness: Brightness.dark,
    backgroundColor: ColorHelper.quaternary,
    scaffoldBackgroundColor: ColorHelper.quaternary,
    highlightColor: ColorHelper.quaternary,
    splashColor: ColorHelper.quaternary,
    primarySwatch: Colors.amber,
    textTheme: GoogleFonts.arimoTextTheme().copyWith(
    ),
);
