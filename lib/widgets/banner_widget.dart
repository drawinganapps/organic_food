import 'package:flutter/cupertino.dart';
import 'package:organic_food_app/helper/color_helper.dart';

class BannerWidget extends StatelessWidget {
  const BannerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: EdgeInsets.only(bottom: heightSize * 0.02),
          width: widthSize,
          height: heightSize * 0.2,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15)
          ),
          clipBehavior: Clip.hardEdge,
          child: Image.asset('assets/images/banner.png', fit: BoxFit.cover),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: 5,
              width: widthSize * 0.08,
              decoration: BoxDecoration(
                color: ColorHelper.primary,
                borderRadius: BorderRadius.circular(10)
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: widthSize * 0.02),
              height: 6,
              width: 6,
              decoration: BoxDecoration(
                  color: ColorHelper.secondary.withOpacity(0.5),
                  shape: BoxShape.circle
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: widthSize * 0.02),
              height: 6,
              width: 6,
              decoration: BoxDecoration(
                  color: ColorHelper.secondary.withOpacity(0.5),
                  shape: BoxShape.circle
              ),
            )
          ],
        )
      ],
    );
  }
  
}