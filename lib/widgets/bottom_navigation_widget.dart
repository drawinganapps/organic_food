import 'package:flutter/material.dart';
import 'package:organic_food_app/helper/color_helper.dart';

class BottomNavigationWidget extends StatelessWidget {
  final int selectedMenu;

  const BottomNavigationWidget({super.key, required this.selectedMenu});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return SizedBox(
      height: heightSize * 0.09,
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.center,
            colors: [
              ColorHelper.quaternary,
              ColorHelper.tertiary,
            ],
          ),
        ),
        padding:
            EdgeInsets.only(left: widthSize * 0.03, right: widthSize * 0.03),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      ColorHelper.primary,
                      ColorHelper.secondary,
                    ],
                  ),
                  borderRadius: BorderRadius.circular(15)
              ),
              padding: EdgeInsets.only(
                top: 5,
                bottom: 5,
                left: widthSize * 0.03,
                right: widthSize * 0.03
              ),
              child: Row(
                children: [
                  Icon(Icons.home_rounded,
                      size: 25,
                      color: ColorHelper.white),
                  Container(
                    margin: EdgeInsets.only(left: widthSize * 0.01),
                    child: Text('Home', style: TextStyle(
                      color: ColorHelper.white
                    )),
                  )
                ],
              ),
            ),
            Container(
              decoration: const BoxDecoration(shape: BoxShape.circle),
              padding: EdgeInsets.all(heightSize * 0.015),
              child: Icon(Icons.explore_rounded,
                  size: selectedMenu == 3 ? 45 : 35,
                  color: ColorHelper.primary.withOpacity(0.5)),
            ),
            Container(
              decoration: const BoxDecoration(shape: BoxShape.circle),
              padding: EdgeInsets.all(heightSize * 0.015),
              child: Icon(Icons.notifications_rounded,
                  size: selectedMenu == 1 ? 45 : 35,
                  color: ColorHelper.primary.withOpacity(0.5)),
            ),
            Container(
              decoration: const BoxDecoration(shape: BoxShape.circle),
              padding: EdgeInsets.all(heightSize * 0.015),
              child: Icon(Icons.favorite_rounded,
                  size: selectedMenu == 4 ? 45 : 35,
                  color: ColorHelper.primary.withOpacity(0.5)),
            ),
          ],
        ),
      ),
    );
  }
}
