import 'package:flutter/cupertino.dart';
import 'package:organic_food_app/helper/color_helper.dart';
import 'package:organic_food_app/models/filter_model.dart';

class FilterWidget extends StatelessWidget {
  final bool isSelected;
  final FilterModel filter;
  const FilterWidget({super.key, required this.isSelected, required this.filter});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      padding: const EdgeInsets.only(bottom: 5),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: isSelected ? ColorHelper.primary : ColorHelper.quaternary, width: 2)
        )
      ),
      child: Text(filter.name, style: TextStyle(
        fontSize: widthSize * 0.04,
        color: isSelected ? ColorHelper.primary : ColorHelper.dark.withOpacity(0.4),
        fontWeight: isSelected ? FontWeight.w600 : FontWeight.w500
      )),
    );
  }
}