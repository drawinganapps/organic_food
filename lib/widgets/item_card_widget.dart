import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:organic_food_app/helper/color_helper.dart';
import 'package:organic_food_app/models/item_model.dart';

class ItemCardWidget extends StatelessWidget {
  final ItemModel item;
  const ItemCardWidget({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.all(widthSize * 0.02),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: ColorHelper.quaternary,
        boxShadow: [
          BoxShadow(
            color: ColorHelper.tertiary,
            blurRadius: 20.0, // soften the shadow
            spreadRadius: 5.0, //extend the shadow
            offset: const Offset(
              10.0, // Move to right 10  horizontally
              10.0, // Move to bottom 10 Vertically
            ),
          )
        ]
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset('assets/images/${item.icon}', fit: BoxFit.cover, width: widthSize * 0.35),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(item.name, style: TextStyle(
                color: ColorHelper.dark,
                fontWeight: FontWeight.w600,
                fontSize: 18
              )),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Icon(Icons.star_rounded, color: ColorHelper.yellow),
                      Text(item.rate.toString(), style: TextStyle(
                        color: ColorHelper.dark.withOpacity(0.4)
                      ))
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text('\$', style: TextStyle(
                        color: ColorHelper.primary,
                        fontSize: 9
                      )),
                      Text(item.price.toString(), style: TextStyle(
                        color: ColorHelper.primary,

                      )),
                      Text(' Kg', style: TextStyle(
                        color: ColorHelper.dark.withOpacity(0.4),
                        fontSize: 11
                      ))
                    ],
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }

}