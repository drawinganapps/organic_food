import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:organic_food_app/helper/color_helper.dart';

class ItemQuantityWidget extends StatelessWidget {
  const ItemQuantityWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Row(
      children: [
        Container(
          padding: EdgeInsets.all(widthSize * 0.015),
          decoration: BoxDecoration(
              color: ColorHelper.grey,
              borderRadius: BorderRadius.circular(10)
          ),
          child: Icon(Icons.remove_rounded, color: ColorHelper.primary),
        ),
        Container(
          margin: EdgeInsets.only(left: widthSize * 0.03, right: widthSize * 0.03),
          child: Text('3', style: TextStyle(
              color: ColorHelper.dark,
              fontSize: 24
          )),
        ),
        Container(
          padding: EdgeInsets.all(widthSize * 0.015),
          decoration: BoxDecoration(
              color: ColorHelper.grey,
              borderRadius: BorderRadius.circular(10)
          ),
          child: Icon(Icons.add_rounded, color: ColorHelper.primary),
        ),
      ],
    );
  }

}