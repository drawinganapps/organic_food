import 'package:flutter/material.dart';
import 'package:organic_food_app/helper/color_helper.dart';

class SearchWidget extends StatelessWidget {
  const SearchWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          height: heightSize * 0.06,
          width: widthSize * 0.73,
          child: TextField(
            decoration: InputDecoration(
                contentPadding: const EdgeInsets.only(
                    left: 15, right: 15, top: 20, bottom: 20),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25),
                    borderSide: BorderSide(
                        color: ColorHelper.tertiary, width: 0)),
                filled: true,
                fillColor: ColorHelper.tertiary,
                hintText: 'Search fresh food',
                prefixIcon:
                Icon(Icons.search, color: ColorHelper.secondary, size: 25),
                hintStyle: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: ColorHelper.dark.withOpacity(0.5))),
          ),
        ),
        Container(
          height: heightSize * 0.05,
          width: heightSize * 0.05,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  ColorHelper.primary,
                  ColorHelper.secondary,
                ],
              ),
            borderRadius: BorderRadius.circular(15)
          ),
          child: const Icon(Icons.tune_rounded, size: 35),
        )
      ],
    );
  }
}
